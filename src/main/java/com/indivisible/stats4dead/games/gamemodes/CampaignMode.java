package com.indivisible.stats4dead.games.gamemodes;

/**
 * Campaign Mode.<br/>
 *
 * 4 Survivors play against AI SpecialInfected.<br/>
 * No InfectedHalf, only SurvivorHalf.<br/>
 *
 * Ignoring this GameMode in favour of Versus Mode.
 */
public class CampaignMode {
}
