package com.indivisible.stats4dead.games.gamemodes;

/**
 * Enum for the different popular game versions / configs. <br/>
 * For keeping the different stats separate.
 */
public enum GameType
{

    ///////////////////////////////////////////////////////
    ////    enum
    ///////////////////////////////////////////////////////

    // DO NOT EDIT INT VALUES (after release)

    NONE(0),            // invalid or null value

    vanilla(1),         // no mods
    custom(10),          // custom modded server

    confogol(20),       // basic confogol mod (still in dev?)
    promod(25),         // north american config
    equlibrium(30);     // international config


    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private int index;


    ///////////////////////////////////////////////////////
    ////    init and getter
    ///////////////////////////////////////////////////////

    private GameType(int index)
    {
        this.index = index;
    }

    public int getIndex()
    {
        return index;
    }


    ///////////////////////////////////////////////////////
    ////    assign type from supplied value
    ///////////////////////////////////////////////////////

    public GameType fromValue(int value)
    {
        switch (value)
        {
            case 1:
                return vanilla;
            case 10:
                return custom;

            case 20:
                return confogol;
            case 25:
                return promod;
            case 30:
                return equlibrium;

            default:
            case 0:
                return NONE;
        }
    }


}
