package com.indivisible.stats4dead.exception;

/**
 * Connection drop, corrupted stream, general Exception for JSONny things.
 */
public class InvalidJSONObjectException
        extends Exception
{

    public InvalidJSONObjectException()
    {
        super();
    }

    public InvalidJSONObjectException(String message)
    {
        super(message);
    }

    public InvalidJSONObjectException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public InvalidJSONObjectException(Throwable cause)
    {
        super(cause);
    }

    protected InvalidJSONObjectException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
