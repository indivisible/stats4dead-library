package com.indivisible.stats4dead.config;

import java.io.File;

/**
 * GameConfiguration. <br/>
 *
 * Settings read in from a config file to tweak the various aspects of a Game. <br/>
 * Please see the example file for how to
 */
public class GameConfiguration
        extends Configuration
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    // Survivors
    private static int healthSurvivor;

    // SpecialInfected
    private static int maxHealthHunter;
    private static int maxHealthSmoker;
    private static int maxHealthBoomer;
    private static int maxHealthJockey;
    private static int maxHealthCharger;
    private static int maxHealthSpitter;
    private static int maxHealthTank;
    private static int maxHealthWitch;

    // health


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public GameConfiguration(File configurationFile)
    {
        super(configurationFile);
        //
    }
}
