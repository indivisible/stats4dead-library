package com.indivisible.stats4dead.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

/**
 * Base Class for Configuration. <br/>
 *
 * To be read from file
 */
public abstract class Configuration
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private File configFile;
    private JSONObject jsonObj;


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public Configuration(File configFile)
    {
        this.configFile = configFile;
        if (configExists())
        {
            this.jsonObj = getObjectFromFile();
        }
    }

    private boolean configExists()
    {
        return this.configFile.exists() && this.configFile.canRead();
    }


    ///////////////////////////////////////////////////////
    ////    JSON
    ///////////////////////////////////////////////////////

    private JSONObject getObjectFromFile()
    {
        JSONObject jsonObject = new JSONObject();
        JSONParser parser = new JSONParser(JSONParser.MODE_PERMISSIVE);
        FileInputStream fis = null;
        try
        {
            fis = new FileInputStream(configFile);
            jsonObject = (JSONObject) parser.parse(fis);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (fis != null)
            {
                try
                {
                    fis.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        return jsonObject;
    }


    public void printRootParts()
    {
        for (String key : this.jsonObj.keySet())
        {
            System.out.println("\t" + key);
        }
    }

}
