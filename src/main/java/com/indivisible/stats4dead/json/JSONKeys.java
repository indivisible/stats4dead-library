package com.indivisible.stats4dead.json;

/**
 * Static collection of JSON keys used throughout the app.
 */
public class JSONKeys
{

    ///////////////////////////////////////////////////////
    ////    single keys
    ///////////////////////////////////////////////////////

    // players
    public static final String PLAYERS_ROOT = "players";
    public static final String PLAYERS_STEAM_ID = "steamID";
    public static final String PLAYERS_NICKNAME = "nickname";
    public static final String PLAYERS_ALIASES = "aliases";

    // survivors
    public static final String SURVIVORS_ROOT = "survivors";
    public static final String SURVIVORS_MAX_HELATH = "maxHp";
    public static final String SURVIVORS_MAX_DOWNED_HEALTH = "maxDownHP";
    public static final String SURVIVORS_CHARACTER = "character";

    // infected
    public static final String INFECTED_ROOT = "infected";
    public static final String INFECTED_TYPE = "type";
    public static final String INFECTED_PLAYER_ID = "playerID";
    public static final String INFECTED_MAX_HEALTH = "maxHP";

    // campaigns
    public static final String CAMPAIGNS_ROOT = "campaigns";
    public static final String CAMPAIGNS_NAME_SHORT = "nameShort";
    public static final String CAMPAIGNS_NAME_LONG = "nameLong";
    public static final String CAMPAIGNS_MAPS_ARRAY = "maps";
    public static final String CAMPAIGNS_CUSTOM_DISTANCE = "customDistance";

    // game aka root bit
    public static final String GAME_ID = "gameID";
    public static final String GAME_TIMESTAMP = "time";


    ///////////////////////////////////////////////////////
    ////    many keys
    ///////////////////////////////////////////////////////

    public static final String[] GAME_HAS_KEYS = {
            PLAYERS_ROOT,       //
            SURVIVORS_ROOT,     //
            INFECTED_ROOT,      //
            CAMPAIGNS_ROOT,     //
    };

    public static final String[] GAME_NEEDS_ATTRIBUTES = {
            GAME_ID,            //
            GAME_TIMESTAMP,     //
    };

}
