package com.indivisible.stats4dead.json;

import net.minidev.json.JSONObject;

/**
 * Created by indiv on 30/07/14.
 */
public interface JSONable
{

    /**
     * Test if the supplied key matches this implementing class' key.
     * 
     * @param key
     * @return
     */
    public boolean keysMatch(String key);

    /**
     * Populate this Object from the values in the supplied JSONObject
     * 
     * @param jsonNewObject
     * @return
     */
    public boolean populate(JSONObject jsonNewObject);

    /**
     * <p>
     * Update this Object using the possibly partial values stored in the
     * supplied JSONObject.
     * </p>
     * <p>
     * Should not be used to initialize a new Object.
     * </p>
     * 
     * 
     * @param jsonUpdateObject
     * @return
     */
    public boolean update(JSONObject jsonUpdateObject);

    /**
     * Test whether valid and appropriate JSONObject exists
     * 
     * @return true for success, false for failure, null for none; <br/>
     *         TODO: messy, enum.
     */
    public Boolean hasValidJSON();

    /**
     * Encode an implementing class (and all children) in a compressed format
     * suitable for transmission.
     * 
     * @return
     */
    public String encodeCompressed();

    /**
     * Encode an implementing class (and all children) in an uncompressed, more
     * readable format.
     * 
     * @return
     */
    public String encodeReadable();

}
