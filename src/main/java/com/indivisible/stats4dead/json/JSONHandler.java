package com.indivisible.stats4dead.json;

import java.io.*;
import java.util.Map;
import java.util.Set;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

/**
 * General, static methods to deal with JSON jazz.
 */
public class JSONHandler
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    public static final int PARSER_MODE = JSONParser.MODE_PERMISSIVE;


    ///////////////////////////////////////////////////////
    ////    streams
    ///////////////////////////////////////////////////////

    public static JSONObject jsonObjectFromFile(String filePath)
    {
        File jsonFile = new File(filePath);
        if (isValidFile(jsonFile))
        {
            FileInputStream fis = null;
            try
            {
                fis = new FileInputStream(jsonFile);
                return jsonObjectFromStream(fis);
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            finally
            {
                if (fis != null)
                {
                    try
                    {
                        fis.close();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
        else
        {
            System.out.println("Cannot read file: " + filePath);
        }
        return null;
    }

    public static JSONObject jsonObjectFromStream(InputStream is)
    {
        JSONParser parser = new JSONParser(PARSER_MODE);
        try
        {
            return (JSONObject) parser.parse(is);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        catch (ClassCastException e)
        {
            e.printStackTrace();
        }
        return null;
    }


    ///////////////////////////////////////////////////////
    ////    JSON Validating
    ///////////////////////////////////////////////////////

    public static boolean isValidAndNotEmptyJSONObject(JSONObject jsonObject)
    {
        return jsonObject != null && !jsonObject.isEmpty();
    }

    public static boolean ensureContainsKeys(JSONObject jsonObject, String[] keys)
    {
        for (String key : keys)
        {
            if (!jsonObject.containsKey(key))
            {
                return false;
            }
        }
        return true;
    }


    ///////////////////////////////////////////////////////
    ////    json reading
    ///////////////////////////////////////////////////////

    public static int getNumberOfChildren(JSONObject jsonObject)
    {
        if (isValidAndNotEmptyJSONObject(jsonObject))
        {
            return jsonObject.keySet().size();
        }
        else
        {
            return 0;
        }
    }

    public static Set<String> getKeys(JSONObject jsonObject)
    {
        if (isValidAndNotEmptyJSONObject(jsonObject))
        {
            return jsonObject.keySet();
        }
        return null;
    }

    public static JSONObject getchildObject(JSONObject jsonObject, String childKey)
    {
        if (isValidAndNotEmptyJSONObject(jsonObject))
        {
            return (JSONObject) jsonObject.get(childKey);
        }
        return null;
    }


    ///////////////////////////////////////////////////////
    ////    json writing
    ///////////////////////////////////////////////////////

    public JSONObject addToObject(JSONObject jsonObject, String key, Object value)
    {
        jsonObject.put(key, value);
        return jsonObject;
    }

    public JSONObject jsonObjectFromMap(Map<String, Object> map)
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.putAll(map);
        return jsonObject;
    }

    ///////////////////////////////////////////////////////
    ////    utils
    ///////////////////////////////////////////////////////

    private static boolean isValidFile(File testFile)
    {
        return testFile.exists() && testFile.canRead();
    }


}
