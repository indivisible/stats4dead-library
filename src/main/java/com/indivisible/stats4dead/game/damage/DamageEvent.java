package com.indivisible.stats4dead.game.damage;

import com.indivisible.stats4dead.game.players.Player;

/**
 * 
 */
public class DamageEvent
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private Player playerFrom;
    private Player playerTo;
    private float damageDone;
    private DamageType damageType;


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public DamageEvent(Player playerFrom, Player playerTo, int damageDone,
            DamageType damageType)
    {
        this.playerFrom = playerFrom;
        this.playerTo = playerTo;
        this.damageDone = damageDone;
        this.damageType = damageType;
    }


    ///////////////////////////////////////////////////////
    ////    gets & sets
    ///////////////////////////////////////////////////////


    public Player getPlayerFrom()
    {
        return playerFrom;
    }

    public Player getPlayerTo()
    {
        return playerTo;
    }

    public float getDamageDone()
    {
        return damageDone;
    }

    public DamageType getDamageType()
    {
        return damageType;
    }
}
