package com.indivisible.stats4dead.game.damage;

/**
 * Created by indiv on 29/07/14.
 */
public interface DamageType
{

    public String getName();

    public String getShortName();

}
