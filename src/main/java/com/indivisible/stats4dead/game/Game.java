package com.indivisible.stats4dead.game;

import net.minidev.json.JSONObject;

import com.indivisible.stats4dead.exception.InvalidJSONObjectException;
import com.indivisible.stats4dead.game.campaign.Campaign;
import com.indivisible.stats4dead.game.players.Team;
import com.indivisible.stats4dead.json.JSONHandler;
import com.indivisible.stats4dead.json.JSONKeys;
import com.indivisible.stats4dead.json.JSONable;

/**
 * A Game. Root
 */
public class Game
        implements JSONable
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    //private Players players;
    //private Survivors survivors;
    //private Infected infected;
    //private
    private Campaign campaign = null;

    private Team teamA = null;
    private Team teamB = null;

    private String gameId = null;


    private String selfKey = null;
    private JSONObject selfJSON = null;


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public Game(JSONObject gameJSON) throws InvalidJSONObjectException
    {
        this.selfJSON = gameJSON;
        if (!hasValidJSON())
        {
            throw new InvalidJSONObjectException("Error validating Game JSON");
        }
        if (!populate(selfJSON))
        {
            throw new InvalidJSONObjectException("Error parsing Game JSON");
        }
    }


    ///////////////////////////////////////////////////////
    ////    gets & sets
    ///////////////////////////////////////////////////////

    public Campaign getCampaign()
    {
        return this.campaign;
    }

    public void setCampaign(Campaign campaign)
    {
        this.campaign = campaign;
    }


    ///////////////////////////////////////////////////////
    ////    jsonable
    ///////////////////////////////////////////////////////


    @Override
    public boolean keysMatch(String key)
    {
        return this.selfKey.equals(key);
    }

    @Override
    public boolean populate(JSONObject jsonNewObject)
    {
        //TODO
        return false;
    }

    @Override
    public boolean update(JSONObject jsonUpdateObject)
    {
        //TODO
        return false;
    }

    @Override
    public Boolean hasValidJSON()
    {
        if (JSONHandler.isValidAndNotEmptyJSONObject(selfJSON))
        {
            return JSONHandler.ensureContainsKeys(selfJSON, JSONKeys.GAME_NEEDS_ATTRIBUTES);
        }
        return false;
    }

    @Override
    public String encodeCompressed()
    {
        //TODO
        return null;
    }

    @Override
    public String encodeReadable()
    {
        //TODO
        return null;
    }
}
