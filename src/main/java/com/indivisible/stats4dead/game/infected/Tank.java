package com.indivisible.stats4dead.game.infected;

import com.indivisible.stats4dead.game.players.Player;

/**
 * Created by indiv on 06/07/14.
 */
public class Tank
        extends SpecialInfected
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    public static final int MAX_HEALTH = 6000;
    public static final String TAG = "Tank";

    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public Tank(Player player)
    {
        super(SpecialInfectedType.tank, player);
    }

}
