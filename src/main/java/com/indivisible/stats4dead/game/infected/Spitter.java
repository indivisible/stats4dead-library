package com.indivisible.stats4dead.game.infected;

import com.indivisible.stats4dead.game.players.Player;

/**
 * Created by indiv on 06/07/14.
 */
public class Spitter
        extends SpecialInfected
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    public static final int MAX_HEALTH = 100;
    public static final String TAG = "Spitter";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public Spitter(Player player)
    {
        super(SpecialInfectedType.spitter, player);
    }

}
