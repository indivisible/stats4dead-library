package com.indivisible.stats4dead.game.infected;

import net.minidev.json.JSONObject;

import com.indivisible.stats4dead.json.JSONHandler;
import com.indivisible.stats4dead.json.JSONKeys;
import com.indivisible.stats4dead.json.JSONable;
import com.indivisible.stats4dead.game.players.Playable;
import com.indivisible.stats4dead.game.players.Player;

/**
 * Abstract class for Player controlled Special Infected. <br/>
 * Used in Versus, Scavenge and other multi-team game modes.
 */
public abstract class SpecialInfected
        implements Playable, JSONable
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private SpecialInfectedType infectedType;
    private int maxHealth;
    private Player controllingPlayer;

    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public SpecialInfected(SpecialInfectedType infectedType, Player player)
    {
        this.controllingPlayer = player;
        this.infectedType = infectedType;
    }


    ///////////////////////////////////////////////////////
    ////    get & set
    ///////////////////////////////////////////////////////

    public SpecialInfectedType getInfectedType()
    {
        return this.infectedType;
    }

    public int getMaxHealth()
    {
        return this.maxHealth;
    }

    public void setMaxHealth(int maxHealth)
    {
        this.maxHealth = maxHealth;
    }


    ///////////////////////////////////////////////////////
    ////    Playable
    ///////////////////////////////////////////////////////

    @Override
    public Player getPlayer()
    {
        return controllingPlayer;
    }

    @Override
    public void setPlayer(Player player)
    {
        this.controllingPlayer = player;
    }


    ///////////////////////////////////////////////////////
    ////    JSONable
    ///////////////////////////////////////////////////////

    @Override
    public JSONObject toJSON()
    {
        JSONObject infectedJSON = new JSONObject();
        infectedJSON.put(JSONKeys.INFECTED_PLAYER_ID, getPlayer().getSteamID());
        infectedJSON.put(JSONKeys.INFECTED_TYPE, getInfectedType().name());
        return infectedJSON;
    }

    @Override
    public JSONable fromJSON(JSONObject jsonObject)
    {
        if (JSONHandler.isValidAndNotEmptyJSONObject(jsonObject))
        {
            SpecialInfectedType infectedType = null;
            String playerId = null;

            try
            {
                if (jsonObject.containsKey(JSONKeys.INFECTED_TYPE))
                {
                    infectedType = SpecialInfectedType.valueOf((String) jsonObject
                            .get(JSONKeys.INFECTED_TYPE));
                }
                if (jsonObject.containsKey(JSONKeys.INFECTED_PLAYER_ID))
                {
                    playerId = (String) jsonObject.get(JSONKeys.INFECTED_PLAYER_ID);
                }
            }
            catch (ClassCastException e)
            {
                System.out.println("Failed to parse JSON for SpecialInfected");
                e.printStackTrace();
            }
        }
        return null;
    }
}
