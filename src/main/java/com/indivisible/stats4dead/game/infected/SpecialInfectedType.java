package com.indivisible.stats4dead.game.infected;

/**
 * Created by indiv on 06/07/14.
 */
public enum SpecialInfectedType
{

    common,     // L4D1, L4D2
    hunter,     // L4D1, L4D2
    smoker,     // L4D1, L4D2
    boomer,     // L4D1, L4D2
    jockey,     // L4D2
    charger,    // L4D2
    spitter,    // L4D2
    tank,       // L4D1, L4D2
    witch;      // L4D1, L4D2
}
