package com.indivisible.stats4dead.game.infected;

import com.indivisible.stats4dead.game.players.Player;

/**
 * Created by indiv on 06/07/14.
 */
public class Boomer
        extends SpecialInfected
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    public static final int MAX_HEALTH = 50;
    public static final String TAG = "Boomer";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public Boomer(Player player)
    {
        super(SpecialInfectedType.boomer, player);
    }

}
