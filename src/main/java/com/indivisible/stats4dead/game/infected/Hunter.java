package com.indivisible.stats4dead.game.infected;

import com.indivisible.stats4dead.game.players.Player;

/**
 * Created by indiv on 06/07/14.
 */
public class Hunter
        extends SpecialInfected
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    public static final int MAX_HEALTH = 250;
    public static final String TAG = "Hunter";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public Hunter(Player player)
    {
        super(SpecialInfectedType.hunter, player);
    }

}
