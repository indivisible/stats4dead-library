package com.indivisible.stats4dead.game.infected;

import com.indivisible.stats4dead.game.players.Player;

/**
 * Created by indiv on 31/07/14.
 */
public class SpecialFactory
{

    public static SpecialInfected newSpecialInfected(SpecialInfectedType infectedType,
                                                     Player player)
    {
        switch (infectedType)
        {
            case common:
                return new CommonInfected();
            case hunter:
                return new Hunter(player);
            case smoker:
                return new Smoker(player);
            case boomer:
                return new Boomer(player);
            case jockey:
                return new Jockey(player);
            case charger:
                return new Charger(player);
            case spitter:
                return new Spitter(player);
            case tank:
                return new Tank(player);
            case witch:
                return new Witch();
            default:
                return null;
        }
    }
}
