package com.indivisible.stats4dead.game.infected;

import com.indivisible.stats4dead.game.players.Player;

/**
 * Created by indiv on 06/07/14.
 */
public class Jockey
        extends SpecialInfected
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    public static final int MAX_HEALTH = 325;
    public static final String TAG = "Jockey";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public Jockey(Player player)
    {
        super(SpecialInfectedType.jockey, player);
    }

}
