package com.indivisible.stats4dead.game.infected;

import com.indivisible.stats4dead.game.players.Player;

/**
 * Created by indiv on 30/07/14.
 */
public class CommonInfected
        extends SpecialInfected
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    public static final int MAX_HEALTH = 100;
    public static final String TAG = "Common";

    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public CommonInfected()
    {
        super(SpecialInfectedType.common, new Player("AI", "AI"));
    }

}
