package com.indivisible.stats4dead.game.infected;

import com.indivisible.stats4dead.game.players.Player;

/**
 * Created by indiv on 30/07/14.
 */
public class Witch
        extends SpecialInfected
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    public static final int MAX_HEALTH = 1000;
    public static final String TAG = "Witch";

    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public Witch()
    {
        super(SpecialInfectedType.witch, new Player("AI", "AI"));
    }

}
