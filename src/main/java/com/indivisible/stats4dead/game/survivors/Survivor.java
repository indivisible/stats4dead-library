package com.indivisible.stats4dead.game.survivors;

import com.indivisible.stats4dead.json.JSONable;
import com.indivisible.stats4dead.game.players.Player;

/**
 * Class for a Player on the Survivor Team. <br/>
 * Used in all game versions.
 */
public class Survivor
        extends Player
        implements JSONable
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private int permHealth = 100;
    private int tempHealth = 0;
    private int downedHealth = 300;
    private boolean isAlive = true;

    private int damageToSpecialInfected = 0;
    private int damageToTank = 0;
    private int damageToWitch = 0;
    private int friendlyFire = 0;

    private int commonInfectedKilled = 0;
    private int specialInfectedKilled = 0;

    private int headshotsToCommonInfected = 0;
    private int headshotsToSpecialInfected = 0;

    private int hunterSkeets = 0;
    private int chargerLevels = 0;
    private int witchCrowns = 0;

    private int incapacitations = 0;
    private int pillsUsed = 0;
    private int medPacksUsed = 0;
    private int pipesThrown = 0;
    private int molotovsThrown = 0;

    private Player player;

    public static final int MAX_HEALTH = 100;
    public static final String TAG = "Survivor";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public Survivor(String steamId, String name)
    {
        super(steamId, name);
    }


    ///////////////////////////////////////////////////////
    ////    gets & sets
    ///////////////////////////////////////////////////////

    //TODO: all the gets & sets

    public int getPermHealth()
    {
        return permHealth;
    }

    public void setPermHealth(int permHealth)
    {
        this.permHealth = permHealth;
    }

    public int getTempHealth()
    {
        return tempHealth;
    }

    public void setTempHealth(int tempHealth)
    {
        this.tempHealth = tempHealth;
    }

    public int getDownedHealth()
    {
        return downedHealth;
    }

    public void setDownedHealth(int downedHealth)
    {
        this.downedHealth = downedHealth;
    }

    public boolean isAlive()
    {
        return isAlive;
    }

    public void setAlive(boolean isAlive)
    {
        this.isAlive = isAlive;
    }

    public int getDamageToTank()
    {
        return damageToTank;
    }

    public void incrementDamageToTank(int damageToTank)
    {
        this.damageToTank += damageToTank;
    }

    public int getDamageToSpecialInfected()
    {
        return damageToSpecialInfected;
    }

    public void setDamageToSpecialInfected(int damageToSpecialInfected)
    {
        this.damageToSpecialInfected = damageToSpecialInfected;
    }

    public int getDamageToWitch()
    {
        return damageToWitch;
    }

    public void setDamageToWitch(int damageToWitch)
    {
        this.damageToWitch = damageToWitch;
    }

    public int getFriendlyFire()
    {
        return friendlyFire;
    }

    public void setFriendlyFire(int friendlyFire)
    {
        this.friendlyFire = friendlyFire;
    }

    public int getCommonInfectedKilled()
    {
        return commonInfectedKilled;
    }

    public void setCommonInfectedKilled(int commonInfectedKilled)
    {
        this.commonInfectedKilled = commonInfectedKilled;
    }

    public int getSpecialInfectedKilled()
    {
        return specialInfectedKilled;
    }

    public void setSpecialInfectedKilled(int specialInfectedKilled)
    {
        this.specialInfectedKilled = specialInfectedKilled;
    }

    public int getHeadshotsToCommonInfected()
    {
        return this.headshotsToCommonInfected;
    }

    public void setHeadshotsToCommonInfected(int headshotsToCommonInfected)
    {
        this.headshotsToCommonInfected = headshotsToCommonInfected;
    }

    public int getHeadshotsToSpecialInfected()
    {
        return this.headshotsToSpecialInfected;
    }

    public void setHeadshotsToSpecialInfected(int headshotsToSpecialInfected)
    {
        this.headshotsToSpecialInfected = headshotsToSpecialInfected;
    }

    public int getHunterSkeets()
    {
        return hunterSkeets;
    }

    public void setHunterSkeets(int hunterSkeets)
    {
        this.hunterSkeets = hunterSkeets;
    }

    public int getChargerLevels()
    {
        return chargerLevels;
    }

    public void setChargerLevels(int chargerLevels)
    {
        this.chargerLevels = chargerLevels;
    }

    public int getWitchCrowns()
    {
        return witchCrowns;
    }

    public void setWitchCrowns(int witchCrowns)
    {
        this.witchCrowns = witchCrowns;
    }

    public int getIncapacitations()
    {
        return incapacitations;
    }

    public void setIncapacitations(int incapacitations)
    {
        this.incapacitations = incapacitations;
    }

    public int getPillsUsed()
    {
        return pillsUsed;
    }

    public void setPillsUsed(int pillsUsed)
    {
        this.pillsUsed = pillsUsed;
    }

    public int getMedPacksUsed()
    {
        return medPacksUsed;
    }

    public void setMedPacksUsed(int medPacksUsed)
    {
        this.medPacksUsed = medPacksUsed;
    }

    public int getPipesThrown()
    {
        return pipesThrown;
    }

    public void setPipesThrown(int pipesThrown)
    {
        this.pipesThrown = pipesThrown;
    }

    public int getMolotovsThrown()
    {
        return molotovsThrown;
    }

    public void setMolotovsThrown(int molotovsThrown)
    {
        this.molotovsThrown = molotovsThrown;
    }

    public Player getPlayer()
    {
        return player;
    }

    public void setPlayer(Player player)
    {
        this.player = player;
    }


    ///////////////////////////////////////////////////////
    ////    increments
    ///////////////////////////////////////////////////////

    //TODO: increment the counter fields

}
