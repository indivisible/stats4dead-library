package com.indivisible.stats4dead.game.players;

/**
 * Created by indiv on 30/07/14.
 */
public interface Playable
{

    public Player getPlayer();

    public void setPlayer(Player player);
}
