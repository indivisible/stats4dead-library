package com.indivisible.stats4dead.game.players;

/**
 * Created by indiv on 31/07/14.
 */
public enum TeamType
{
    survivors,      // 
    infected,       //
    spectators;     //
}
