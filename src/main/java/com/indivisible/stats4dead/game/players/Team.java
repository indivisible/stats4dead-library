package com.indivisible.stats4dead.game.players;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Team<br/>
 *
 * A Team consists of 4 Players.<br/>
 * The Players may be either Survivors or Special Infected.
 */
public class Team
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private String name = null;
    private String abbreviation = null;
    private List<Player> playingPlayers;
    private HashMap<String, Integer> historyPlayers;

    private static final String DEFAULT_NAME = "Team";
    private static final String DEFAULT_ABBREVIATION = "NA";
    private static final String NAME_SPACER = " ";

    //TODO: Make the number of playingPlayers a configuration option
    private static final int MAX_PLAYERS = 4;


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    /**
     * Create a new, empty Team.
     * 
     * @param teamName
     */
    public Team(String teamName)
    {
        this.name = teamName;
        playingPlayers = new ArrayList<Player>();
        historyPlayers = new HashMap<String, Integer>();
    }

    /**
     * Create a new, empty Team.
     * 
     * @param teamName
     * @param teamAbbreviation
     */
    public Team(String teamName, String teamAbbreviation)
    {
        this(teamName);
        this.abbreviation = teamAbbreviation;
    }


    ///////////////////////////////////////////////////////
    ////    gets & sets
    ///////////////////////////////////////////////////////

    /**
     * Get this Team's full (long) name.
     * 
     * @return A String name or default if one not set.
     */
    public String getTeamName()
    {
        if (name != null)
            return name;
        else
            return DEFAULT_NAME;
    }

    /**
     * Set this Team's full (long) name.
     * 
     * @param teamName
     */
    public void setTeamName(String teamName)
    {
        this.name = teamName;
    }

    /**
     * Get this Team's abbreviated (short) form.
     * 
     * @return A String abbreviation for this Team or a default if one not set.
     */
    public String getTeamAbbreviation()
    {
        if (abbreviation != null)
            return abbreviation;
        else
            return DEFAULT_ABBREVIATION;
    }

    /**
     * Set this Team's abbreviated (short) name.
     * 
     * @param teamAbbreviation
     */
    public void setTeamAbbreviation(String teamAbbreviation)
    {
        this.abbreviation = teamAbbreviation;
    }

    /**
     * Get the Players currently playing for this Team.
     * 
     * @return
     */
    public List<Player> getPlayingPlayers()
    {
        return this.playingPlayers;
    }

    /**
     * Set the Players currently playing for this Team.
     * 
     * @param players
     * @return
     */
    public boolean setActivePlayers(List<Player> players)
    {
        if (players.size() <= MAX_PLAYERS)
        {
            this.playingPlayers = players;
            return true;
        }
        else
        {
            System.out.println("Team can only contain 4 active Players. Team unchanged.");
            return false;
        }
    }

    /**
     * Get any additional Players that had played for this Team but have been
     * replaced.
     * 
     * @return
     */
    public List<Player> getHistoryPlayers()
    {
        return this.historyPlayers;
    }

    /**
     * Set the collection of Players that had played for this Team and had been
     * replaced.
     * 
     * @param players
     */
    public void setHistoryPlayers(List<Player> players)
    {
        this.historyPlayers = players;
    }

    /* FIXME
    OK, this is a mess.
    Rethink with SQL backing. encode from SQL, update to SQL, time ranges in SQL
     */

    ///////////////////////////////////////////////////////
    ////    checks
    ///////////////////////////////////////////////////////

    /**
     * Get the current number of Players on this Team.
     * 
     * @return
     */
    public int size()
    {
        return playingPlayers.size();
    }

    /**
     * Check whether the Team has all available slots filled.
     * 
     * @return
     */
    public boolean isTeamFull()
    {
        return size() == MAX_PLAYERS;
    }

    /**
     * Check if there is room for more Players on this Team.
     * 
     * @return
     */
    public boolean doesTeamHaveFreeSlots()
    {
        return size() < MAX_PLAYERS;
    }

    /**
     * Test whether a Player is on this Team.
     * 
     * @param player
     * @return
     */
    public boolean isOnTeam(Player player)
    {
        return playingPlayers.contains(player);
    }


    ///////////////////////////////////////////////////////
    ////    manipulate team
    ///////////////////////////////////////////////////////

    /**
     * Add a Player to this Team. <br/>
     * A Team can only contain max 4 Players. <br/>
     * For a larger container for more Players use TeamRoster.
     *
     * @param newPlayer
     * @return true on successful add, false if Team already full.
     */
    public boolean addToPlaying(Player newPlayer)
    {
        addToRosterOrIncrement(newPlayer);
        if (isTeamFull())
        {
            System.out.println("Team already has 4 Players, use Team.substitute() to replace");
            return false;
        }
        else if (playingPlayers.contains(newPlayer))
        {
            System.out.println("That player is already on the Team.");
            return true;
        }
        else
        {
            return playingPlayers.add(newPlayer);
        }
    }

    /**
     * Add a number of Players at once. <br/>
     *
     * @param newPlayers
     * @return
     */
    public boolean addToPlaying(List<Player> newPlayers)
    {
        if (newPlayers.size() > MAX_PLAYERS)
        {
            System.out.println("Too many Players being added to Team. Max is 4. None added.");
            return false;
        }
        else if (newPlayers.size() + size() > MAX_PLAYERS)
        {
            System.out.println("Not enough free slots for all supplied Players. None added.");
            return false;
        }
        else
        {
            for (int i = 0; i < playingPlayers.size() || i < MAX_PLAYERS; i++)
            {
                addToPlaying(playingPlayers.get(i));
            }
            return true;
        }
    }

    /**
     * Replace one Player on a Team with another.
     * 
     * @param oldPlayer
     *        Player to be replaced
     * @param newPlayer
     *        Player to be substituted in
     * @return true on successful substitution, false if failed (due to
     *         oldPlayer not found on Team).
     */
    public boolean substitute(Player oldPlayer, Player newPlayer)
    {
        int oldPlayerIndex = playingPlayers.indexOf(oldPlayer);
        if (oldPlayerIndex < 0)
        {
            System.out.println("Player to sub out not found on Team: " + oldPlayer.getName());
            return false;
        }
        else
        {
            playingPlayers.remove(oldPlayer);
            playingPlayers.add(oldPlayerIndex, newPlayer);
            return true;
        }
    }


    ///////////////////////////////////////////////////////
    ////    stringgy bits
    ///////////////////////////////////////////////////////

    @Override
    public String toString()
    {
        if (getTeamName() != null)
        {
            return getTeamName();
        }
        else if (getTeamAbbreviation() != null)
        {
            return getTeamAbbreviation();
        }
        else
        {
            return DEFAULT_NAME;
        }
    }

    /**
     * Get a fixed width String identifying this Team. <br/>
     * (not guaranteed to be unique)
     * 
     * @param width
     *        The length of the desired String to be returned.
     * @param tryIncludeAbbreviation
     *        Include an abbreviation if one exists.
     * @return An exact length String representation of this Team.
     */
    public String fixedWidthName(int width, boolean tryIncludeAbbreviation)
    {
        StringBuilder sb = new StringBuilder();
        String abbr = getTeamAbbreviation();
        if (tryIncludeAbbreviation)
        {
            sb.append(abbr).append(NAME_SPACER);
        }
        sb.append(getTeamName());
        return String.format("%1$" + width + "s", sb.toString());
    }

}
