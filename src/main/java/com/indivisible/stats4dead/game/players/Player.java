package com.indivisible.stats4dead.game.players;

import java.util.Arrays;
import java.util.List;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import com.indivisible.stats4dead.json.JSONHandler;
import com.indivisible.stats4dead.json.JSONKeys;
import com.indivisible.stats4dead.json.JSONable;

/**
 * Base class for any Player controlled entity.
 */
public class Player
        implements Comparable, JSONable
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private String steamID = null;
    private String name = null;
    private List<String> aliases = null;


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public Player(String steamID, String name)
    {
        this(steamID, name, new String[0]);
    }

    public Player(String steamID, String name, String[] aliases)
    {
        this.steamID = steamID;
        this.name = name;
        setAliases(aliases);
    }


    ///////////////////////////////////////////////////////
    ////    gets & sets
    ///////////////////////////////////////////////////////

    public String getSteamID()
    {
        return this.steamID;
    }

    public void setSteamID(String steamID)
    {
        this.steamID = steamID;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String[] getAliases()
    {
        return (String[]) this.aliases.toArray();
    }

    public void setAliases(String[] aliases)
    {
        this.aliases = Arrays.asList(aliases);
    }

    public boolean addToAliases(String alias)
    {
        if (this.aliases.contains(alias))
        {
            return false;
        }
        else
        {
            return this.aliases.add(alias);
        }
    }

    public boolean addToAliases(String[] newAliases)
    {
        for (String alias : newAliases)
        {
            this.aliases.add(alias);
        }
        return true;
    }


    ///////////////////////////////////////////////////////
    ////    jsonable
    ///////////////////////////////////////////////////////

    @Override
    public JSONObject toJSON()
    {
        JSONObject jsonPlayer = new JSONObject();
        jsonPlayer.put(JSONKeys.PLAYERS_STEAM_ID, getSteamID());
        jsonPlayer.put(JSONKeys.PLAYERS_NICKNAME, getName());

        JSONArray jsonAliases = new JSONArray();
        jsonAliases.add(getName());
        for (String alias : getAliases())
        {
            jsonAliases.add(alias);
        }
        jsonPlayer.put(JSONKeys.PLAYERS_ALIASES, jsonAliases);

        return jsonPlayer;
    }

    @Override
    public JSONable fromJSON(JSONObject jsonObject)
    {
        if (JSONHandler.isValidAndNotEmptyJSONObject(jsonObject))
        {
            try
            {
                String steamID_ = null;
                String nickname_ = null;
                String[] aliases_ = null;

                if (jsonObject.containsKey(JSONKeys.PLAYERS_STEAM_ID))
                {
                    steamID_ = (String) jsonObject.get(JSONKeys.PLAYERS_STEAM_ID);
                }
                if (jsonObject.containsKey(JSONKeys.PLAYERS_NICKNAME))
                {
                    nickname_ = (String) jsonObject.get(JSONKeys.PLAYERS_NICKNAME);
                }
                if (jsonObject.containsKey(JSONKeys.PLAYERS_ALIASES))
                {
                    aliases_ = (String[]) jsonObject.get(JSONKeys.PLAYERS_ALIASES);
                }

                if (steamID_ != null && nickname_ != null)
                {
                    Player player = new Player(steamID_, nickname_);
                    if (aliases_ != null)
                    {
                        player.setAliases(aliases_);
                    }
                    return player;
                }
            }
            catch (ClassCastException e)
            {
                System.out.println("Failed to parse JSON for Player");
                e.printStackTrace();
            }
        }
        return null;
    }


    ///////////////////////////////////////////////////////
    ////    equals, compare
    ///////////////////////////////////////////////////////


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;
        if (!steamID.equals(player.steamID)) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        return steamID.hashCode();
    }

    @Override
    public int compareTo(Object object)
    {
        //RET: implement some compare
        return 0;
    }

}
