package com.indivisible.stats4dead.game.campaign;

import java.util.List;

/**
 * A Campaign. <br/>
 *
 * This is the encompassing class for a full game. <br/>
 * A Campaign has multiple Chapters.
 */
public class Campaign
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private List<Chapter> childChapters;


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public Campaign()
    {
        //
    }


}
