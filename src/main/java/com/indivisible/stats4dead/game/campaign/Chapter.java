package com.indivisible.stats4dead.game.campaign;

import java.util.List;

/**
 * A Chapter. <br/>
 *
 * A Campaign is split into multiple Chapters.<br/>
 * A Chapter has two Halves (Half)s
 */
public class Chapter
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private List<Half> childHalves;


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public Chapter()
    {

    }


    ///////////////////////////////////////////////////////
    ////    gets & sets
    ///////////////////////////////////////////////////////

    public List<Half> getChildHalves()
    {
        return childHalves;
    }

    public void setChildHalves(List<Half> childHalves)
    {
        this.childHalves = childHalves;
    }
}
